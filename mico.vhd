
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use work.p_wires.all;

entity mico is
  port (rst,clk : in bit);
end mico;

architecture functional of mico is

  component mem_prog is                 -- no arquivo mem.vhd
    port (ender : in  reg6;
          instr : out reg32);
  end component mem_prog;

  component display is                  -- neste arquivo
    port (rst,clk : in bit;
          enable  : in bit;
          data    : in reg32);
  end component display;

  component ULA is                      -- neste arquivo
    port (fun : in reg4;
          alfa,beta : in  reg32;
          gama      : out reg32);
  end component ULA;
 
  component R is                        -- neste arquivo
    port (clk         : in  bit;
          wr_en       : in  bit;
          r_a,r_b,r_c : in  reg4;
          A,B         : out reg32;
          C           : in  reg32);
  end component R;

  component RAM is                      -- neste arquivo
    port (rst, clk : in  bit;
          sel      : in  bit;           -- ativo em 1
          wr       : in  bit;           -- ativo em 1
          ender    : in  reg16;
          data_inp : in  reg32;
          data_out : out reg32);
  end component RAM;

  component count32up is
   port(rel, rst, ld, en: in  bit;
        D:               in  reg32;
        opcode:          in  reg4;
        const:           in  reg16;
        Q:               out reg32);
  end component count32up;

  component inv is
   port(A : in bit;
        S : out bit);
  end component inv;

  type t_control_type is record
    extZero    : bit;      -- estende com zero=1, com sinal=0
    selBeta    : bit;      -- seleciona fonte para entrada B da ULA
    wr_display : bit;      -- atualiza display=1
    selNxtIP   : bit;      -- seleciona fonte do incremento do IP
    wr_reg     : bit;      -- atualiza banco de registradores
    selC       : bit;      -- seleciona fonte da escrita no reg destino
    mem_sel    : bit;      -- habilita acesso a RAM
    mem_wr     : bit;      -- habilita escrita na RAM
  end record;

  type t_control_mem is array (0 to 15) of t_control_type;
  
  constant ctrl_table : t_control_mem := (
  --extZ sBeta wrD sIP wrR selC  M_sel M_wr
    ('0','0', '0', '0','0', '0', '0', '0'),            -- NOP
    ('0','0', '0', '0','1', '0', '0', '0'),            -- ADD
    ('0','0', '0', '0','1', '0', '0', '0'),            -- SUB
    ('0','0', '0', '0','1', '0', '0', '0'),            -- MUL
    ('0','0', '0', '0','1', '0', '0', '0'),            -- AND
    ('0','0', '0', '0','1', '0', '0', '0'),            -- OR
    ('0','0', '0', '0','1', '0', '0', '0'),            -- XOR
    ('0','0', '0', '0','1', '0', '0', '0'),            -- NOT
    ('1','1', '0', '0','1', '0', '0', '0'),            -- ORI
    ('0','1', '0', '0','1', '0', '0', '0'),            -- ADDI
    ('0','1', '0', '0','1', '0', '1', '0'),            -- LD
    ('0','1', '0', '0','0', '1', '1', '1'),            -- ST
    ('0','0', '1', '0','0', '0', '0', '0'),            -- SHOW
    ('0','1', '0', '1','0', '0', '0', '0'),            -- JUMP
    ('0','1', '0', '0','0', '0', '0', '0'),            -- BRANCH
    ('0','0', '0', '1','0', '0', '0', '0'));           -- HALT
  

  constant HALT : bit_vector := x"f";


  signal extZero, selBeta, wr_display, selNxtIP, wr_reg, selC, notrst : bit;
  signal mem_sel, mem_wr : bit;

  signal instr, A, B, C, beta, gama, extended, ula_D, mem_D, s_RAM, ip32, proxIP : reg32;
  signal this  : t_control_type;
  signal const, ip, ext_zeros, ext_sinal : reg16;
  signal opcode, r_a, r_b, r_c : reg4;
  signal i_opcode : natural range 0 to 15;
  
begin  -- mico

  -- IP --
  -- nega o rst pra usar no contador
  u_invr : inv port map(rst, notrst);
  -- contador para ir pro proximo ip
  u_cont: count32up port map (clk, notrst, selNxtIP, '1', extended, opcode, const, proxIP);
  -- se o SelnxtIP for 1 ou se for branch e a e b forem iguais, IP recebe const
  -- se o nxtIP for 0 ou se for branch e a e b forem diferentes, IP recebe IP + 1
  ip <= const when ((A = B) and (opcode = x"e")) or (SelNxtIP = '1') 
  else proxIP(15 downto 0);

  -- memoria de programa contem somente 64 palavras
  U_mem_prog: mem_prog port map(ip(5 downto 0), instr);

  opcode <= instr(31 downto 28);
  i_opcode <= BV2INT4(opcode);          -- indice do vetor DEVE ser inteiro
  const    <= instr(15 downto 0);

  -- registradores recebem a instrucao
  r_a <= instr(27 downto 24);
  r_b <= instr(23 downto 20);
  r_c <= instr(19 downto 16);
  
  this <= ctrl_table(i_opcode);         -- sinais de controle

  extZero    <= this.extZero;
  selBeta    <= this.selBeta;
  wr_display <= this.wr_display;
  selNxtIP   <= this.selNxtIP;
  wr_reg     <= this.wr_reg;
  selC       <= this.selC;
  mem_sel    <= this.mem_sel;
  mem_wr     <= this.mem_wr;

  -- extende o const de 16 para 32 bits
  -- quando o extensor de zeros estiver ligado e se a instr for positiva concatena com 0
  -- quando o extensor de zeros estiver desligado e a instr for negativa concatena com 1
  extended <= x"0000" & const(15 downto 0) when (extZero = '0' and instr(15) = '0')
  else x"ffff" & const(15 downto 0);

  U_regs: R port map (clk, wr_reg, r_a, r_b, r_c, A, B, C);

  U_ULA: ULA port map (opcode, A, beta, gama);

  -- mux do registrador, beta
  with selBeta select beta <=
   B when '0',
   extended when '1';

  U_mem: RAM port map (rst, clk, mem_sel, mem_wr, gama(15 downto 0), B, s_RAM);
  
  -- mux da ula e da memoria 
  with selC select C <=
  gama when '0',
  s_RAM when '1';
  
  -- nao altere esta linha
  U_display: display port map (rst, clk, wr_display, A);
  
  assert opcode /= HALT
    report LF & LF & "simulation halted: " & 
    "ender = "&integer'image(BV2INT16(ip))&" = "&BV16HEX(ip)&LF
    severity failure;
  
end functional;
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity ULA is
  port (fun : in reg4;
        alfa,beta : in  reg32;
        gama      : out reg32);
end ULA;

architecture behaviour of ULA is

component and_32 is
  port (A, B : in reg32;
        S    : out reg32);
end component and_32;

component or_32 is
  port(A, B : in  reg32; 
       S    : out reg32); 
end component or_32;

component xor_32 is
  port(A, B : in  reg32; 
       S    : out reg32); 
end component xor_32;

component inv_32 is
  port(A : in  reg32; 
       S : out reg32); 
end component inv_32;

component adderCadeia is
  port(inpA, inpB : in reg32;
       outC : out reg32;
       vem  : in bit;
       vai  : out bit);
end component adderCadeia;

component mult16x16 is
  port(A, B : in  reg16;  
       prod : out reg32); 
end component mult16x16;

signal s_and, s_or, s_xor, s_inv, s_add, s_sub, s_mul : reg32;
signal notbeta, notalfa, alfaM, betaM, alfaC, betaC, mulC, notres, result : reg32;
signal vai_add, vai_sub, vai_ca, vai_cb, vai_mul : bit;

begin  -- behaviour
-- -----------------------------------------------------------------------
--inverte alfa e beta para serem utilizados em algumas operacoes
o_invb: inv_32 port map (beta, notbeta);
o_inva: inv_32 port map (alfa, notalfa);

-- complemento de 2 de alfa e beta para serem utilizados em algumas operacoes
o_cb : adderCadeia port map (notalfa, x"00000001", alfaC, '0', vai_ca);
o_ca : adderCadeia port map (notbeta, x"00000001", betaC, '0', vai_cb);

-- -----------------------------------------------------------------------
-- se a op for add
o_add : adderCadeia port map (alfa, beta, s_add, '0', vai_add);
-- -----------------------------------------------------------------------
-- se a op for sub
--o_sub : adderCadeia port map (alfa, betaC, s_sub, '0', vai_sub);
-- -----------------------------------------------------------------------
-- se a op for mult

-- se o alfa for positivo, escolhe ele
-- se nao, pega o complemento do alfa
 with alfa(31) select alfaM <=
  alfa when '0',
  alfaC when '1';
-- se o beta for positivo, escolhe ele
-- se nao, pega o complemento do beta
with beta(31) select betaM <=
  beta when '0',
  betaC when '1';
 -- multiplica alfa e beta positivos
o_mul : mult16x16 port map (alfaM(15 downto 0),betaM(15 downto 0), result);
-- faz o complemento do resultado da multiplicacao
o_invr : inv_32 port map (result, notres);
o_cm : adderCadeia port map (notres,  x"00000001", mulC, '0', vai_mul);
-- se alfa OU beta for negativo, o resultado é o complemento da multiplicacao
-- se alfa e beta forem positivos ou negativos, o resultado é o proprio resultado da soma
s_mul <= result when ((alfa(31) xnor beta(31)) = '1') else mulC;
-- -----------------------------------------------------------------------
-- se a op for and
o_and : and_32 port map(alfa, beta, s_and);
-- -----------------------------------------------------------------------
-- se a op for or
o_or : or_32 port map(alfa, beta, s_or);
-- -----------------------------------------------------------------------
-- se a op for xor
o_xor : xor_32 port map(alfa, beta, s_xor);
-- -----------------------------------------------------------------------
-- se a op for not
o_inv : inv_32 port map(alfa, s_inv);
-- -----------------------------------------------------------------------

-- seleciona gama conforme o opcode
with fun select gama <=
  x"00000000" when x"0", -- nop
  s_add when x"1",       -- add
  s_sub when x"2",       -- sub
  s_mul when x"3",       -- mul
  s_and when x"4",       -- and
  s_or when x"5",        -- or
  s_xor when x"6",       -- xor
  s_inv when x"7",       -- not
  s_or when x"8",        -- ori
  s_add when x"9",       -- addi
  s_add when x"a",       -- ld
  s_add when x"b",       -- st
  x"00000000" when x"c", -- show
  x"00000000" when x"d", -- jump
  x"00000000" when x"e", -- branch
  x"00000000" when x"f"; -- halt
  
end behaviour;
-- -----------------------------------------------------------------------

-- and de 32 bits
use work.p_wires.all;

entity and_32 is
  port(A, B : in  reg32; 
       S    : out reg32); 
end and_32;

architecture estrut of and_32 is 
component and2 is
  generic (prop : time := t_and2);
  port(A, B : in  bit;  -- entradas A,B
       S    : out bit); -- saida C
end component and2;
 
begin
  u_a0 : and2 generic map (t_and2) port map(A(0), B(0), S(0));
  u_a1 : and2 generic map (t_and2) port map(A(1), B(1), S(1));
  u_a2 : and2 generic map (t_and2) port map(A(2), B(2), S(2));
  u_a3 : and2 generic map (t_and2) port map(A(3), B(3), S(3));
  u_a4 : and2 generic map (t_and2) port map(A(4), B(4), S(4));
  u_a5 : and2 generic map (t_and2) port map(A(5), B(5), S(5));
  u_a6 : and2 generic map (t_and2) port map(A(6), B(6), S(6));
  u_a7 : and2 generic map (t_and2) port map(A(7), B(7), S(7));
  u_a8 : and2 generic map (t_and2) port map(A(8), B(8), S(8));
  u_a9 : and2 generic map (t_and2) port map(A(9), B(9), S(9));
  u_a10 : and2 generic map (t_and2) port map(A(10), B(10), S(10));
  u_a11 : and2 generic map (t_and2) port map(A(11), B(11), S(11));
  u_a12 : and2 generic map (t_and2) port map(A(12), B(12), S(12));
  u_a13 : and2 generic map (t_and2) port map(A(13), B(13), S(13));
  u_a14 : and2 generic map (t_and2) port map(A(14), B(14), S(14));
  u_a15 : and2 generic map (t_and2) port map(A(15), B(15), S(15));
  u_a16 : and2 generic map (t_and2) port map(A(16), B(16), S(16));
  u_a17 : and2 generic map (t_and2) port map(A(17), B(17), S(17));
  u_a18 : and2 generic map (t_and2) port map(A(18), B(18), S(18));
  u_a19 : and2 generic map (t_and2) port map(A(19), B(19), S(19));
  u_a20 : and2 generic map (t_and2) port map(A(20), B(20), S(20));
  u_a21 : and2 generic map (t_and2) port map(A(21), B(21), S(21));
  u_a22 : and2 generic map (t_and2) port map(A(22), B(22), S(22));
  u_a23 : and2 generic map (t_and2) port map(A(23), B(23), S(23));
  u_a24 : and2 generic map (t_and2) port map(A(24), B(24), S(24));
  u_a25 : and2 generic map (t_and2) port map(A(25), B(25), S(25));
  u_a26 : and2 generic map (t_and2) port map(A(26), B(26), S(26));
  u_a27 : and2 generic map (t_and2) port map(A(27), B(27), S(27));
  u_a28 : and2 generic map (t_and2) port map(A(28), B(28), S(28));
  u_a29 : and2 generic map (t_and2) port map(A(29), B(29), S(29));
  u_a30 : and2 generic map (t_and2) port map(A(30), B(30), S(30));
  u_a31 : and2 generic map (t_and2) port map(A(31), B(31), S(31));

end architecture estrut;

-- -----------------------------------------------------------------------
-- or de 32 bits
use work.p_wires.all;

entity or_32 is
  port(A, B : in  reg32; 
       S    : out reg32); 
end or_32;

architecture estrut of or_32 is 

  component or2 is
    generic (prop : time);
    port(A,B : in bit; 
         S : out bit);
  end component or2;
 
begin
  u_o0 : or2 generic map (t_or2) port map(A(0), B(0), S(0));
  u_o1 : or2 generic map (t_or2) port map(A(1), B(1), S(1));
  u_o2 : or2 generic map (t_or2) port map(A(2), B(2), S(2));
  u_o3 : or2 generic map (t_or2) port map(A(3), B(3), S(3));
  u_o4 : or2 generic map (t_or2) port map(A(4), B(4), S(4));
  u_o5 : or2 generic map (t_or2) port map(A(5), B(5), S(5));
  u_o6 : or2 generic map (t_or2) port map(A(6), B(6), S(6));
  u_o7 : or2 generic map (t_or2) port map(A(7), B(7), S(7));
  u_o8 : or2 generic map (t_or2) port map(A(8), B(8), S(8));
  u_o9 : or2 generic map (t_or2) port map(A(9), B(9), S(9));
  u_o10 : or2 generic map (t_or2) port map(A(10), B(10), S(10));
  u_o11 : or2 generic map (t_or2) port map(A(11), B(11), S(11));
  u_o12 : or2 generic map (t_or2) port map(A(12), B(12), S(12));
  u_o13 : or2 generic map (t_or2) port map(A(13), B(13), S(13));
  u_o14 : or2 generic map (t_or2) port map(A(14), B(14), S(14));
  u_o15 : or2 generic map (t_or2) port map(A(15), B(15), S(15));
  u_o16 : or2 generic map (t_or2) port map(A(16), B(16), S(16));
  u_o17 : or2 generic map (t_or2) port map(A(17), B(17), S(17));
  u_o18 : or2 generic map (t_or2) port map(A(18), B(18), S(18));
  u_o19 : or2 generic map (t_or2) port map(A(19), B(19), S(19));
  u_o20 : or2 generic map (t_or2) port map(A(20), B(20), S(20));
  u_o21 : or2 generic map (t_or2) port map(A(21), B(21), S(21));
  u_o22 : or2 generic map (t_or2) port map(A(22), B(22), S(22));
  u_o23 : or2 generic map (t_or2) port map(A(23), B(23), S(23));
  u_o24 : or2 generic map (t_or2) port map(A(24), B(24), S(24));
  u_o25 : or2 generic map (t_or2) port map(A(25), B(25), S(25));
  u_o26 : or2 generic map (t_or2) port map(A(26), B(26), S(26));
  u_o27 : or2 generic map (t_or2) port map(A(27), B(27), S(27));
  u_o28 : or2 generic map (t_or2) port map(A(28), B(28), S(28));
  u_o29 : or2 generic map (t_or2) port map(A(29), B(29), S(29));
  u_o30 : or2 generic map (t_or2) port map(A(30), B(30), S(30));
  u_o31 : or2 generic map (t_or2) port map(A(31), B(31), S(31));
end architecture estrut;

-- -----------------------------------------------------------------------
-- xor de 32 bits
use work.p_wires.all;

entity xor_32 is
  port(A, B : in  reg32; 
       S    : out reg32); 
end xor_32;

architecture estrut of xor_32 is 

component xor2 is
  port(A, B : in bit;
       S    : out bit);
end component xor2;
 
begin
  u_x0 : xor2 port map(A(0), B(0), S(0));
  u_x1 : xor2 port map(A(1), B(1), S(1));
  u_x2 : xor2 port map(A(2), B(2), S(2));
  u_x3 : xor2 port map(A(3), B(3), S(3));
  u_x4 : xor2 port map(A(4), B(4), S(4));
  u_x5 : xor2 port map(A(5), B(5), S(5));
  u_x6 : xor2 port map(A(6), B(6), S(6));
  u_x7 : xor2 port map(A(7), B(7), S(7));
  u_x8 : xor2 port map(A(8), B(8), S(8));
  u_x9 : xor2 port map(A(9), B(9), S(9));
  u_x10 : xor2 port map(A(10), B(10), S(10));
  u_x11 : xor2 port map(A(11), B(11), S(11));
  u_x12 : xor2 port map(A(12), B(12), S(12));
  u_x13 : xor2 port map(A(13), B(13), S(13));
  u_x14 : xor2 port map(A(14), B(14), S(14));
  u_x15 : xor2 port map(A(15), B(15), S(15));
  u_x16 : xor2 port map(A(16), B(16), S(16));
  u_x17 : xor2 port map(A(17), B(17), S(17));
  u_x18 : xor2 port map(A(18), B(18), S(18));
  u_x19 : xor2 port map(A(19), B(19), S(19));
  u_x20 : xor2 port map(A(20), B(20), S(20));
  u_x21 : xor2 port map(A(21), B(21), S(21));
  u_x22 : xor2 port map(A(22), B(22), S(22));
  u_x23 : xor2 port map(A(23), B(23), S(23));
  u_x24 : xor2 port map(A(24), B(24), S(24));
  u_x25 : xor2 port map(A(25), B(25), S(25));
  u_x26 : xor2 port map(A(26), B(26), S(26));
  u_x27 : xor2 port map(A(27), B(27), S(27));
  u_x28 : xor2 port map(A(28), B(28), S(28));
  u_x29 : xor2 port map(A(29), B(29), S(29));
  u_x30 : xor2 port map(A(30), B(30), S(30));
  u_x31 : xor2 port map(A(31), B(31), S(31));
end architecture estrut;

-- -----------------------------------------------------------------------
-- inversor de 32 bits
use work.p_wires.all;

entity inv_32 is
  port(A : in  reg32; 
       S : out reg32); 
end inv_32;

architecture estrut of inv_32 is 

component inv is
  generic (prop : time := t_inv);
  port(A : in bit;
       S : out bit);
end component inv;
 
begin
  u_i0 : inv generic map (t_inv) port map(A(0), S(0));
  u_i1 : inv generic map (t_inv) port map(A(1), S(1));
  u_i2 : inv generic map (t_inv) port map(A(2), S(2));
  u_i3 : inv generic map (t_inv) port map(A(3), S(3));
  u_i4 : inv generic map (t_inv) port map(A(4), S(4));
  u_i5 : inv generic map (t_inv) port map(A(5), S(5));
  u_i6 : inv generic map (t_inv) port map(A(6), S(6));
  u_i7 : inv generic map (t_inv) port map(A(7), S(7));
  u_i8 : inv generic map (t_inv) port map(A(8), S(8));
  u_i9 : inv generic map (t_inv) port map(A(9), S(9));
  u_i10 : inv generic map (t_inv) port map(A(10), S(10));
  u_i11 : inv generic map (t_inv) port map(A(11), S(11));
  u_i12 : inv generic map (t_inv) port map(A(12), S(12));
  u_i13 : inv generic map (t_inv) port map(A(13), S(13));
  u_i14 : inv generic map (t_inv) port map(A(14), S(14));
  u_i15 : inv generic map (t_inv) port map(A(15), S(15));
  u_i16 : inv generic map (t_inv) port map(A(16), S(16));
  u_i17 : inv generic map (t_inv) port map(A(17), S(17));
  u_i18 : inv generic map (t_inv) port map(A(18), S(18));
  u_i19 : inv generic map (t_inv) port map(A(19), S(19));
  u_i20 : inv generic map (t_inv) port map(A(20), S(20));
  u_i21 : inv generic map (t_inv) port map(A(21), S(21));
  u_i22 : inv generic map (t_inv) port map(A(22), S(22));
  u_i23 : inv generic map (t_inv) port map(A(23), S(23));
  u_i24 : inv generic map (t_inv) port map(A(24), S(24));
  u_i25 : inv generic map (t_inv) port map(A(25), S(25));
  u_i26 : inv generic map (t_inv) port map(A(26), S(26));
  u_i27 : inv generic map (t_inv) port map(A(27), S(27));
  u_i28 : inv generic map (t_inv) port map(A(28), S(28));
  u_i29 : inv generic map (t_inv) port map(A(29), S(29));
  u_i30 : inv generic map (t_inv) port map(A(30), S(30));
  u_i31 : inv generic map (t_inv) port map(A(31), S(31));
end architecture estrut;

-- -----------------------------------------------------------------------

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- somador completo de um bit, modelo estrutural
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE; use IEEE.std_logic_1164.all; use work.p_wires.all;

entity addBit is
  port(bitA, bitB, vem : in bit;    -- entradas A,B,vem-um
       soma, vai       : out bit);  -- saida C,vai-um
end addBit;

architecture estrutural of addBit is 
  component and2 is generic (prop:time);
                      port (A,B: in bit; S: out bit);
  end component and2;

  component or3 is generic (prop:time);
                      port (A,B,C: in bit; S: out bit);
  end component or3;

  component xor3 is generic (prop:time);
                      port (A,B,C: in bit; S: out bit);
  end component xor3;

  signal a1,a2,a3: bit;
begin
  U_xor:  xor3 generic map ( t_xor3 ) port map ( bitA, bitB, vem, soma );

  U_and1: and2 generic map ( t_and2 ) port map ( bitA, bitB, a1 );
  U_and2: and2 generic map ( t_and2 ) port map ( bitA, vem,  a2 );
  U_and3: and2 generic map ( t_and2 ) port map ( vem,  bitB, a3 );
  U_or:   or3  generic map ( t_or3  ) port map ( a1, a2, a3, vai );

end estrutural;
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- somador de 16 bits, sem adiantamento de vai-um
-- Secao 1.6+8.1.2 de RH
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE; use IEEE.std_logic_1164.all; use work.p_wires.all;

entity adderCadeia is
  port(inpA, inpB : in reg32;
       outC : out reg32;
       vem  : in bit;
       vai  : out bit
       );
end adderCadeia;

architecture adderCadeia of adderCadeia is 
  component addBit port(bitA, bitB, vem : in bit;
                        soma, vai       : out bit);       
  end component addBit;

  signal v : reg32;                     -- cadeia de vai-um
  signal r : reg32;                     -- resultado parcial
begin

  -- entrada vem deve estar ligada em '0' para somar, em '1' para subtrair
  U_b0: addBit port map ( inpA(0), inpB(0), vem,  r(0), v(0) );
  U_b1: addBit port map ( inpA(1), inpB(1), v(0), r(1), v(1) );
  U_b2: addBit port map ( inpA(2), inpB(2), v(1), r(2), v(2) );
  U_b3: addBit port map ( inpA(3), inpB(3), v(2), r(3), v(3) );
  U_b4: addBit port map ( inpA(4), inpB(4), v(3), r(4), v(4) );
  U_b5: addBit port map ( inpA(5), inpB(5), v(4), r(5), v(5) );
  U_b6: addBit port map ( inpA(6), inpB(6), v(5), r(6), v(6) );
  U_b7: addBit port map ( inpA(7), inpB(7), v(6), r(7), v(7) );
  U_b8: addBit port map ( inpA(8), inpB(8), v(7), r(8), v(8) );
  U_b9: addBit port map ( inpA(9), inpB(9), v(8), r(9), v(9) );
  U_ba: addBit port map ( inpA(10),inpB(10),v(9), r(10),v(10) );
  U_bb: addBit port map ( inpA(11),inpB(11),v(10),r(11),v(11) );
  U_bc: addBit port map ( inpA(12),inpB(12),v(11),r(12),v(12) );
  U_bd: addBit port map ( inpA(13),inpB(13),v(12),r(13),v(13) );
  U_be: addBit port map ( inpA(14),inpB(14),v(13),r(14),v(14) );
  U_bf: addBit port map ( inpA(15),inpB(15),v(14),r(15),v(15) );
  U_b10: addBit port map ( inpA(16), inpB(16), v(15),  r(16), v(16) );
  U_b11: addBit port map ( inpA(17), inpB(17), v(16), r(17), v(17) );
  U_b12: addBit port map ( inpA(18), inpB(18), v(17), r(18), v(18) );
  U_b14: addBit port map ( inpA(19), inpB(19), v(18), r(19), v(19) );
  U_b15: addBit port map ( inpA(20), inpB(20), v(19), r(20), v(20) );
  U_b16: addBit port map ( inpA(21), inpB(21), v(20), r(21), v(21) );
  U_b17: addBit port map ( inpA(22), inpB(22), v(21), r(22), v(22) );
  U_b18: addBit port map ( inpA(23), inpB(23), v(22), r(23), v(23) );
  U_b19: addBit port map ( inpA(24), inpB(24), v(23), r(24), v(24) );
  U_b1a: addBit port map ( inpA(25),inpB(25),v(24), r(25),v(25) );
  U_b1b: addBit port map ( inpA(26),inpB(26),v(25),r(26),v(26) );
  U_b1c: addBit port map ( inpA(27),inpB(27),v(26),r(27),v(27) );
  U_b1d: addBit port map ( inpA(28),inpB(28),v(27),r(28),v(28) );
  U_b1e: addBit port map ( inpA(29),inpB(29),v(28),r(29),v(29) );
  U_b1f: addBit port map ( inpA(30),inpB(30),v(29),r(30),v(30) );
  U_b13: addBit port map ( inpA(31),inpB(31),v(31),r(31),v(31) );
  
  vai <= v(15);
  outC <= r;
  
end adderCadeia;
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- multiplica por 1: A(15..0)*B(i) => S(16..0)
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE; use IEEE.std_logic_1164.all; use work.p_wires.all;

entity m_p_1 is
  port(A,B : in  reg16;                 -- entradas A,B
       S : in bit;                      -- bit por multiplicar
       R : out reg17);                  -- produto parcial
end m_p_1;

architecture funcional of m_p_1 is 

  component adderAdianta16 is port(inpA, inpB : in bit_vector;
                          outC : out bit_vector;
                          vem  : in  bit;
                          vai  : out bit);
  end component adderAdianta16;

  signal somaAB : reg17;

begin

u_soma : adderAdianta16 port map(A, B , somaAB(15 downto 0), '0', somaAB(16)); 

  -- defina a constante t_mux2 em packageWires.vhd
  R <= somaAB when S = '1' else ('0' & B);

end funcional;
-- -------------------------------------------------------------------


-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- multiplicador combinacional
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE; use IEEE.std_logic_1164.all; use IEEE.numeric_std.all;
use work.p_wires.all;

entity mult16x16 is
  port(A, B : in  reg16;   -- entradas A,B
       prod : out reg32);  -- produto
end mult16x16;

architecture funcional of mult16x16 is 
begin
  prod <= INT2BV32( BV2INT16(A) * BV2INT16(B) );
end funcional;

 architecture estrutural of mult16x16 is 
 
   component m_p_1 is port(A,B : in  bit_vector;   -- reg16
                           S   : in  bit;
                           R   : out bit_vector);  -- reg17
   end component m_p_1;
 
   signal p01,p02,p03,p04,p05,p06,p07,p08: reg17;
   signal p09,p10,p11,p12,p13,p14,p15,p16: reg17;
 
 begin
 
    U_00: m_p_1 port map (A(15 downto 0), x"0000"         , B(0), p01(16 downto 0));
    U_01: m_p_1 port map (A(15 downto 0), p01(16 downto 1), B(1), p02(16 downto 0));
    U_02: m_p_1 port map (A(15 downto 0), p02(16 downto 1), B(2), p03(16 downto 0));
    U_03: m_p_1 port map (A(15 downto 0), p03(16 downto 1), B(3), p04(16 downto 0));
    U_04: m_p_1 port map (A(15 downto 0), p04(16 downto 1), B(4), p05(16 downto 0));
    U_05: m_p_1 port map (A(15 downto 0), p05(16 downto 1), B(5), p06(16 downto 0));
    U_06: m_p_1 port map (A(15 downto 0), p06(16 downto 1), B(6), p07(16 downto 0));
    U_07: m_p_1 port map (A(15 downto 0), p07(16 downto 1), B(7), p08(16 downto 0));
    U_08: m_p_1 port map (A(15 downto 0), p08(16 downto 1), B(8), p09(16 downto 0));
    U_09: m_p_1 port map (A(15 downto 0), p09(16 downto 1), B(9), p10(16 downto 0));
    U_10: m_p_1 port map (A(15 downto 0), p10(16 downto 1), B(10), p11(16 downto 0));
    U_11: m_p_1 port map (A(15 downto 0), p11(16 downto 1), B(11), p12(16 downto 0));
    U_12: m_p_1 port map (A(15 downto 0), p12(16 downto 1), B(12), p13(16 downto 0));
    U_13: m_p_1 port map (A(15 downto 0), p13(16 downto 1), B(13), p14(16 downto 0));
    U_14: m_p_1 port map (A(15 downto 0), p14(16 downto 1), B(14), p15(16 downto 0));
    U_15: m_p_1 port map (A(15 downto 0), p15(16 downto 1), B(15), p16(16 downto 0));
 
   prod <= p16(16 downto 0) & p15(0) & p14(0) & p13(0) & p12(0) & p11(0) & p10(0) & p09(0) & p08(0) & p07(0) & p06(0) & p05(0) & p04(0) & p03(0) & p02(0) & p01(0);
   
 end estrutural;
-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- display: exibe inteiro na saida padrao do simulador
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library IEEE; use std.textio.all;
use work.p_wires.all;

entity display is
  port (rst,clk : in bit;
        enable  : in bit;
        data    : in reg32);
end display;

architecture functional of display is
  file output : text open write_mode is "STD_OUTPUT";
begin  -- functional

  U_WRITE_OUT: process(clk)
    variable msg : line;
  begin
    if falling_edge(clk) and enable = '1' then
      write ( msg, string'(BV32HEX(data)) );
      writeline( output, msg );
    end if;
  end process U_WRITE_OUT;

end functional;
-- ++ display ++++++++++++++++++++++++++++++++++++++++++++++++++++++++



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- memoria RAM, com capacidade de 64K palavras de 32 bits
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
library ieee; use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use work.p_wires.all;

entity RAM is
  port (rst, clk : in  bit;
        sel      : in  bit;          -- ativo em 1
        wr       : in  bit;          -- ativo em 1
        ender    : in  reg16;
        data_inp : in  reg32;
        data_out : out reg32);

  constant DATA_MEM_SZ : natural := 2**16;
  constant DATA_ADDRS_BITS : natural := log2_ceil(DATA_MEM_SZ);

end RAM;

architecture rtl of RAM is

  subtype t_address is unsigned((DATA_ADDRS_BITS - 1) downto 0);

  subtype word is bit_vector(31 downto 0);
  type storage_array is
    array (natural range 0 to (DATA_MEM_SZ - 1)) of word;
  signal storage : storage_array;
begin

  accessRAM: process(rst, clk, sel, wr, ender, data_inp)
    variable u_addr : t_address;
    variable index, latched : natural;

    variable d : reg32 := (others => '0');
    variable val, i : integer;

  begin

    if (rst = '0') and (sel = '1') then -- normal operation

      index := BV2INT16(ender);

      if  (wr = '1') and rising_edge(clk) then

        assert (index >= 0) and (index < DATA_MEM_SZ)
          report "ramWR index out of bounds: " & natural'image(index)
          severity failure;

        storage(index) <= data_inp;

        assert TRUE report "ramWR["& natural'image(index) &"] "
          & BV32HEX(data_inp); -- DEBUG

      else

        assert (index >= 0) and (index < DATA_MEM_SZ)
          report "ramRD index out of bounds: " & natural'image(index)
          severity failure;

        d := storage(index);

        assert TRUE report "ramRD["& natural'image(index) &"] "
          & BV32HEX(d);  -- DEBUG

      end if; -- normal operation

      data_out <= d;

    else

      data_out <= (others=>'0');

    end if; -- is reset?

  end process accessRAM; ------------------------------------------------

end rtl;
--------------------------------------------------------------------------



-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
-- banco de registradores
--          NAO ALTERE ESTE MODELO
-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
use work.p_wires.all;

entity R is
  port (clk         : in  bit;
        wr_en       : in  bit;          -- ativo em 1
        r_a,r_b,r_c : in  reg4;
        A,B         : out reg32;
        C           : in  reg32);
end R;

architecture rtl of R is
  type reg_file is array(0 to 15) of reg32;
  signal reg_file_A : reg_file;
  signal reg_file_B : reg_file;
  signal int_ra, int_rb, int_rc : integer range 0 to 15;
begin

  int_ra <= BV2INT4(r_a);
  int_rb <= BV2INT4(r_b);
  int_rc <= BV2INT4(r_c);

  A <= reg_file_A( int_ra ) when r_a /= b"0000" else
       x"00000000";                        -- reg0 always zero
  B <= reg_file_B( int_rb ) when r_b /= b"0000" else
       x"00000000";

  WRITE_REG_BANKS: process(clk)
  begin
    if rising_edge(clk) then
      if wr_en = '1' and r_c /= b"0000" then
        reg_file_A( int_rc ) <= C;
        reg_file_B( int_rc ) <= C;
      end if;
    end if;
  end process WRITE_REG_BANKS;
  
end rtl;
-- -----------------------------------------------------------------------
